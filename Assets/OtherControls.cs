using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class OtherControls : MonoBehaviourPunCallbacks
{
    public List<MonoBehaviour> toDisable = new List<MonoBehaviour>();

    // Start is called before the first frame update
    void Start()
    {
        if (!photonView.IsMine)
        {
            foreach (var t in toDisable)
            {
                t.enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
