using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CharacterMenuController : MonoBehaviour
{
    public RawImage colorImg;
    public Slider redSlider;
    public Slider greenSlider;
    public Slider blueSlider;
    public TMP_InputField nameField;

    public Renderer lHand;
    public Renderer rHand;
    public Renderer Head;
    public GameObject handLine;

    public GameObject networkManager;

    public GameObject menuXR;

    public GameObject loadingBox;

    private CharacterInfo info;

    private void Start()
    {
        info = CharacterInfo.Instance;
        
    }

    public void ColorChange()
    {
        Debug.Log(redSlider.value + ", " + greenSlider.value + ", " + blueSlider.value);
        Color newColor = new Color(redSlider.value, greenSlider.value, blueSlider.value);

        lHand.material.color = newColor;
        rHand.material.color = newColor;
        Head.material.color = newColor;

        colorImg.color = newColor;
        info.SetCharColor(newColor);
    }

    public void NameChange()
    {
        info.SetName(nameField.text);
    }

    public void JoinRoom()
    {
        StartCoroutine(JoinDelay());      
    }

    public void OpenKeyboard()
    {
        TouchScreenKeyboard.Open("");
    }

    IEnumerator JoinDelay()
    {
        loadingBox.SetActive(true);
        handLine.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        menuXR.SetActive(false);
        Destroy(gameObject);
        networkManager.SetActive(true);
    }


}
