using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class HideDuplicatePun : MonoBehaviourPun
{
    public List<MonoBehaviour> disableScripts;
    public List<Renderer> disableMesh;
    public Renderer playerHead;
    public NetworkPlayerSpawnerVR vrManager;

    // Start is called before the first frame update
    private void Awake()
    {
        vrManager = GameObject.FindGameObjectWithTag("NetworkHide").GetComponent<NetworkPlayerSpawnerVR>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!photonView.IsMine)
        {
            foreach (MonoBehaviour obj in disableScripts)
            {
                obj.enabled = false;
            }
            foreach (Renderer r in disableMesh)
            {
                r.enabled = false;
            }
        }
        else
        {
            playerHead.enabled = false;
        }
    }

    private void OnDestroy()
    {
        Debug.LogWarning("DESTROY");
        foreach(GameObject obj in vrManager.spawnedPlayers)
        {
            obj.SetActive(false);
            obj.SetActive(true);
        }
    }
}
