using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
//The following script is made following this tutorial: https://www.youtube.com/watch?v=KHWuTBmT1oI

public class NetworkPlayerSpawner : MonoBehaviourPunCallbacks
{

    public GameObject spawnedPlayer;

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        GameObject player = PhotonNetwork.Instantiate("Player_VRFree_Network", transform.position, transform.rotation);


    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        PhotonNetwork.Destroy(spawnedPlayer);
    }

}
