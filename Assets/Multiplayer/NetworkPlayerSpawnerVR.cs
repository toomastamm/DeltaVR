using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Photon.Pun;
using Photon.Realtime;
//The following script is made following this tutorial: https://www.youtube.com/watch?v=KHWuTBmT1oI

public class NetworkPlayerSpawnerVR : MonoBehaviourPunCallbacks
{

    public GameObject spawnedPlayer;
    public List<GameObject> spawnedPlayers = new List<GameObject>();
    public TeleportationProvider teleportationProvider;
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        GameObject player = PhotonNetwork.Instantiate("XR Rig_Network", transform.position, transform.rotation);
        if (CharacterInfo.Instance.GetName() == "") player.name = player.GetPhotonView().ViewID.ToString();
        else player.name = CharacterInfo.Instance.GetName();
        spawnedPlayers.Add(player);

        if (player.GetPhotonView().IsMine)
        {
            teleportationProvider.system = player.GetComponent<LocomotionSystem>();
        }
        
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        PhotonNetwork.Destroy(spawnedPlayer);
        
    }

}
