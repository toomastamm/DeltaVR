using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.Utilities;
#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad] // Automatically register in editor.
#endif
[DisplayStringFormat("{modifier}+{twoDVector}")]
public class AxisWithModifiersComposite : InputBindingComposite<Vector2>
{
    [InputControl(layout = "2DVector")] public int twoDVector;

    [InputControl(layout = "Button")] public int modifier;

    public override Vector2 ReadValue(ref InputBindingCompositeContext context)
    {
        //Debug.Log("Modifier: " + context.ReadValue<float>(modifier));
        //Debug.Log("Vector2: " + context.ReadValue<Vector2, Vector2MagnitudeComparer>(twoDVector));

        Vector2 toReturn = context.ReadValue<float>(modifier) == 0f ? Vector2.zero : context.ReadValue<Vector2, Vector2MagnitudeComparer>(twoDVector);
        
        Debug.Log("Composite raw return: " + toReturn);
        return toReturn;
    }

    static AxisWithModifiersComposite()
    {
        InputSystem.RegisterBindingComposite<AxisWithModifiersComposite>();
    }

    [RuntimeInitializeOnLoadMethod]
    static void Init()
    {
    } // Trigger static constructor.
}