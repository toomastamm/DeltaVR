using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ColorChanger : MonoBehaviourPun
{
    public List<GameObject> changeObjects;


    private void Awake()
    {
        if (!photonView.IsMine)
        {
            gameObject.SetActive(false);
        }
    }


    public void ChangeColorPun()
    {
        photonView.RPC("ChangeColor", RpcTarget.AllBuffered);
    }
}
