using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportOverride : MonoBehaviour
{

    private XRIDefaultInputActions input;
    private Vector2 axis;
    private bool rayEnabled;
    public MonoBehaviour ray;

    private void Awake()
    {
        input = new XRIDefaultInputActions();
        input.XRIRightHand.TeleportModeActivate.performed += ctx => CallRay();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CallRay()
    {
        if (rayEnabled)
        {
            ray.enabled = false;
            rayEnabled = false;
        }
        else
        {
            ray.enabled = true;
            rayEnabled = true;
        }
    }

    private void OnEnable()
    {
        input.Enable();
    }

    private void OnDisable()
    {
        input.Disable();
    }
}
