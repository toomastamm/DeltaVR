using System.Collections;
using System.Collections.Generic;
using Breakout;
using UnityEngine;

public class DestroyBallForcefield : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        BreakoutBall ball = other.gameObject.GetComponent<BreakoutBall>();

        if (ball == null) return;
        Events.ReduceBalls();
        Destroy(ball.gameObject);
    }
}
