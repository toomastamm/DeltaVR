using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicSpeedTransfer : MonoBehaviour
{
    public float amplification = 1f;
    
    private Vector3 _oldPosition;
    private Vector3 _velocity;

    // Start is called before the first frame update
    void Start()
    {
        _oldPosition = transform.position;
        _velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = transform.position;
        Vector3 posDiff = newPosition - _oldPosition;
        _velocity = posDiff / Time.deltaTime;
        _oldPosition = newPosition;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody.isKinematic) return;
        
        other.rigidbody.AddRelativeForce(_velocity * amplification, ForceMode.Impulse);
    }
}