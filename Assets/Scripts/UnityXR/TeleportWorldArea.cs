﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace UnityXR
{
    public class TeleportWorldArea : TeleportationArea
    {
        // Override the teleport request to use the world rotation and not our gameObject's transform
        protected override bool GenerateTeleportRequest(XRBaseInteractor interactor, RaycastHit raycastHit,
            ref TeleportRequest teleportRequest)
        {
            teleportRequest.destinationPosition = raycastHit.point;
            teleportRequest.matchOrientation = MatchOrientation.WorldSpaceUp; // use the area transform for data.
            teleportRequest.destinationRotation = Quaternion.identity;
            return true;
        }
    }
}