using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

public class CustomSnapTurnProvider : ActionBasedSnapTurnProvider
{
    [SerializeField]
    [Tooltip("The Input System Action that will be used to read Snap Turn data from the left hand controller. Must be a Value Vector2 Control.")]
    InputActionProperty m_allowNavigation;
    /// <summary>
    /// The Input System Action that will be used to read Snap Turn data sent from the left hand controller. Must be a <see cref="InputActionType.Value"/> <see cref="Vector2Control"/> Control.
    /// </summary>
    public InputActionProperty allowNavigation
    {
        get => m_allowNavigation;
        set => SetInputActionProperty(ref m_allowNavigation, value);
    }
    
    void SetInputActionProperty(ref InputActionProperty property, InputActionProperty value)
    {
        if (Application.isPlaying)
            property.DisableDirectAction();

        property = value;

        if (Application.isPlaying && isActiveAndEnabled)
            property.EnableDirectAction();
    }

    protected override Vector2 ReadInput()
    {
        return m_allowNavigation.action.IsPressed() ? base.ReadInput() : Vector2.zero;
    }
}
