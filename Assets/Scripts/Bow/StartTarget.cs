using UnityEngine;
using Photon.Pun;

namespace Bow
{
    public class StartTarget : MonoBehaviourPun
    {
        public ArcheryRange archeryRange;
        public Canvas textCanvas;
        private MeshRenderer _meshRenderer;
        private BoxCollider _boxCollider;

        // Start is called before the first frame update
        void Start()
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _boxCollider = GetComponent<BoxCollider>();
        }

        
        public void OnArrowHitPun()
        {
            HideTarget();
            archeryRange.StartRound();
        }
        [PunRPC]
        private void HideTargetPun()
        {
            _meshRenderer.enabled = false;
            _boxCollider.enabled = false;
            textCanvas.enabled = false;
        }
        [PunRPC]
        public void ShowTargetPun()
        {
            _meshRenderer.enabled = true;
            _boxCollider.enabled = true;
            textCanvas.enabled = true;
        }

        public void OnArrowHit(Arrow arrow)
        {
            if (arrow == null) return;

            Destroy(arrow.gameObject);
            OnArrowHitPun();
        }

        private void HideTarget()
        {
            photonView.RPC("HideTargetPun", RpcTarget.AllBuffered);
        }

        public void ShowTarget()
        {
            photonView.RPC("ShowTargetPun", RpcTarget.AllBuffered);
        }
    }
}