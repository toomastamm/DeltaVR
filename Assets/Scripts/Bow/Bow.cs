﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Bow
{
    public class Bow : XRGrabInteractable
    {
        private Animator _animator = null;
        private Puller _puller = null;
        private static readonly int Blend = Animator.StringToHash("Blend");

        protected override void Awake()
        {
            base.Awake();
            _animator = GetComponent<Animator>();
            _puller = GetComponentInChildren<Puller>();
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (updatePhase != XRInteractionUpdateOrder.UpdatePhase.Dynamic) return;
            if (!isSelected) return;

            AnimateBow(_puller.PullAmount);
        }

        private void AnimateBow(float value)
        {
            _animator.SetFloat(Blend, value);
        }
    }
}