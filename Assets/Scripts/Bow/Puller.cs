﻿using Audio;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Bow
{
    public class Puller : XRBaseInteractable
    {
        public float PullAmount { get; private set; } = 0.0f;
        public AudioClipGroup pullSounds;
        public AudioClipGroup maxPullSounds;


        public Transform start = null;
        public Transform end = null;

        private XRBaseInteractor _pullingInteractor = null;

        protected override void OnSelectEntered(XRBaseInteractor interactor)
        {
            base.OnSelectEntered(interactor);
            _pullingInteractor = interactor;
        }

        protected override void OnSelectExited(XRBaseInteractor interactor)
        {
            base.OnSelectExited(interactor);
            _pullingInteractor = null;
            PullAmount = 0.0f;
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (updatePhase != XRInteractionUpdateOrder.UpdatePhase.Dynamic) return;
            if (!isSelected) return;

            Vector3 pullPosition = _pullingInteractor.transform.position;

            float newPull = CalculatePull(pullPosition);
            if (PullAmount == 0 & newPull > 0)
            {
                pullSounds.Play();
            }
            
            if (PullAmount < 0.98f & newPull >= 0.98f)
            {
                maxPullSounds.Play();
            }
            
            PullAmount = newPull;
        }

        private float CalculatePull(Vector3 pullPosition)
        {
            var startPosition = start.position;
            Vector3 pullDirection = pullPosition - startPosition;
            Vector3 targetDirection = end.position - startPosition;
            float maxLength = targetDirection.magnitude;

            targetDirection.Normalize();
            float pullValue = Vector3.Dot(pullDirection, targetDirection) / maxLength;

            return Mathf.Clamp(pullValue, 0, 1);
        }
    }
}