using TMPro;
using UnityEngine;

namespace Bow
{
    public class PointsText : MonoBehaviour
    {
        public TMP_Text text;
        public float destroyTime = 2f;
        public float upSpeed = 2f;

        private float _destroyOnTime;

        private void Start()
        {
            _destroyOnTime = Time.time + destroyTime;
        }

        public void Update()
        {
            if (_destroyOnTime <= Time.time)
            {
                Destroy(gameObject);
            }

            float step = upSpeed * Time.deltaTime;
            var position = transform.position;
            transform.position = Vector3.MoveTowards(position,
                new Vector3(position.x, position.y + 1f, position.z), step);
        }

        public void SetPoints(float points)
        {
            text.text = $"+{points}";
        }
    }
}