﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Bow
{
    public class Quiver : XRSocketInteractor
    {
        public GameObject arrowPrefab = null;
        private Vector3 _attachOffset = Vector3.zero;

        protected override void Awake()
        {
            base.Awake();
            CreateAndSelectArrow();
            SetAttachOffset();
        }

        protected override void OnSelectExited(XRBaseInteractable interactable)
        {
            base.OnSelectExited(interactable);
            CreateAndSelectArrow();
        }

        private void CreateAndSelectArrow()
        {
            Arrow arrow = CreateArrow();
            interactionManager.ForceSelect(this, arrow);
        }

        private Arrow CreateArrow()
        {
            var transform1 = transform;
            GameObject arrowObject = Instantiate(arrowPrefab, transform1.position - _attachOffset, transform1.rotation);
            return arrowObject.GetComponent<Arrow>();
        }

        private void SelectArrow(Arrow arrow)
        {
            OnSelectEntered(arrow);
            arrow.OnSelectEntered(this);
        }

        private void SetAttachOffset()
        {
            if (selectTarget is XRGrabInteractable interactable)
            {
                _attachOffset = interactable.attachTransform.localPosition;
            }
        }
    }
}