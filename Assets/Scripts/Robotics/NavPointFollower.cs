using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class NavPointFollower : MonoBehaviour
{
    public NavPoint startNavPoint;
    public float rotationSpeed = 1f;
    public float speed = 1f;

    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var transform1 = transform;
        var position = transform1.position;
        _rigidbody.MovePosition(position + transform1.forward * (speed * Time.deltaTime));
        
        var direction = (startNavPoint.transform.position - position).normalized;

        direction.y = 0;
        
        var lookRotation = Quaternion.LookRotation(direction);
        
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);

        
        if (Vector3.Distance(transform.position, startNavPoint.transform.position) < 0.3f)
        {
            startNavPoint = startNavPoint.GetNext();
        }
    }
}
