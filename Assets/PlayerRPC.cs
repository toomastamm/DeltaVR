using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class PlayerRPC : MonoBehaviourPunCallbacks
{
    public List<GameObject> changeObjects;
    public TMP_Text playerTag;

    [PunRPC]
    void ChangeText()
    {
        playerTag.text = photonView.ViewID.ToString();
    }

    [PunRPC]
    void ChangeColor(Vector3 color)
    {
        foreach (GameObject go in changeObjects)
        {
            go.GetComponent<Renderer>().material.color = new Color(color.x,color.y, color.z);
        }
    }


    private void Awake()
    {
        if (photonView.IsMine) photonView.RPC("ChangeColor", RpcTarget.AllBuffered, new Vector3(CharacterInfo.Instance.GetCharColor().r, CharacterInfo.Instance.GetCharColor().g, CharacterInfo.Instance.GetCharColor().b));

    }



}
