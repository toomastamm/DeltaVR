using Photon.Pun;
using UnityEngine;

public class MenuController : MonoBehaviourPun
{
    public GameObject MainPanel;
    public GameObject ColorPicker;
    public GameObject TeleportPanel;

    public GameObject Player;

    public VRControls vrControls;

    private float cooldownTime;
    private bool cooldown;


    // Start is called before the first frame update
    void Awake()
    {
        cooldown = false;
        vrControls = new VRControls();
        if (!photonView.IsMine)
        {
            vrControls.Disable();
            gameObject.SetActive(false);
        }
        else
        {
            vrControls.Other.Menu.performed += ctx => MainMenu();
            Debug.Log("VRCONTROLS");
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(cooldown && Time.time - cooldownTime < 1f)
        {
            cooldown = false;
        }
    }

    public void ColorMenu()
    {
        if (!cooldown)
        {
            MainPanel.SetActive(false);
            ColorPicker.SetActive(true);
            cooldownTime = Time.time;
            cooldown = true;
        }
    }

    public void MainMenu()
    {
        if (!cooldown)
        {
            Debug.Log("METHOD");
            MainPanel.SetActive(!MainPanel.activeInHierarchy);
            ColorPicker.SetActive(false);
            TeleportPanel.SetActive(false);
            cooldownTime = Time.time;
            cooldown = true;
            /*if (MainPanel.activeInHierarchy)
            {
                Cursor.lockState = CursorLockMode.Confined;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }*/
        }
    }

    public void TeleportMenu()
    {
        MainPanel.SetActive(false);
        TeleportPanel.SetActive(true);
    }

    public void Teleport(string teleportTo)
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Teleport");
        foreach(GameObject obj in objs)
        {
            if(obj.name == teleportTo) Player.transform.position = obj.transform.position;
        }
        
    }


}
