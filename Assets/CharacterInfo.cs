using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInfo : MonoBehaviour
{
    public static CharacterInfo Instance;

    private Color charColor = Color.white;
    private string charName = "";


    public CharacterInfo()
    {
        Instance = this;
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public string GetName()
    {
        return charName;
    }

    public void SetName(string name)
    {
        charName = name;
    }

    public Color GetCharColor()
    {
        return charColor;
    }

    public void SetCharColor(Color color)
    {
        charColor = color;
    }

    
}
