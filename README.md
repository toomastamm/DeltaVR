# DeltaVR

DeltaVR is a virtual reality experience set in the Delta Centre of the University of Tartu. It was designed and implemented in a Bachelors degree thesis. The proiect used the Delta Building Visualization project as a basis for the building and built upon it, adding missing
details and improving the performance.

DeltaVR - Multiplayer is an extension to DeltaVR allowing players to explore the building together in PCVR, Quest 2 and non-VR versions.

## Student Project Contest Video (Original DeltaVR)

https://www.youtube.com/watch?v=UUGal5J4hac

## Gameplay Sample Footage (Original DeltaVR)

https://youtu.be/AoRN4eluiWY

## Download
2021 version (**No multiplayer**): https://drive.google.com/file/d/1n19_Wa69vCX6s6zKYoSYKirpHcfJHqaM/view?usp=sharing

2022 version (**Multiplayer**): https://gitlab.com/Joonasp1/deltavr-multiplayer-builds
